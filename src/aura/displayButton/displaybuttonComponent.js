({
   openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"   
      document.getElementById('msg').innerHTML='You clicked the Cancel button';
      component.set("v.isOpen", false);
   },
    confirm: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      document.getElementById('msg').innerHTML='You clicked the Confirm button';
      component.set("v.isOpen", false);
   }
})