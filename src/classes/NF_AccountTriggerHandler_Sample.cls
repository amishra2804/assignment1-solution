
 @File Name   NF_ContactTriggerHandler_Sample.cls
 @Description   
 @Author      Recruiter
 @Group       Apex
 @Modification Log 
-------------------------------------------------------------------------------------
 Ver       Date        Author      Modification
 1.0       2017-05-09  Recruiter    Created the fileclass

public with sharing class NF_AccountTriggerHandler_Sample extends NF_AbstractTriggerHandler {
	public override void beforeUpdate(){

	}

	public override void afterUpdate(){

	}

	public override void beforeInsert(){

	}

	public override void afterInsert(){
		
	}

	public override void afterDelete(){

	}

	public override void andFinally(){

	}
}